package main

import (
	_ "ddc_utils/routers"
	"ddc_utils/utils"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
)

func init() {
	utils.Init()
}

func handleSignals(c chan os.Signal) {
	switch <-c {
	case syscall.SIGINT, syscall.SIGTERM:
		fmt.Println("Shutdown quickly, bye...")
	case syscall.SIGQUIT:
		fmt.Println("Shutdown gracefully, bye...")
		// do graceful shutdown
	}

	os.Exit(0)
}

func main() {
	graceful, _ := beego.AppConfig.Bool("Graceful")
	if !graceful {
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
		go handleSignals(sigs)
	}
	logs.SetLogger("file", `{"filename":"logs/logs.log"}`)
	beego.SetStaticPath("/confirm", "public/confirm")

	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
		logs.SetLevel(logs.LevelDebug)
	} else if beego.BConfig.RunMode == "prod" {
		logs.SetLevel(logs.LevelInformational)
	}

	orm.DefaultTimeLoc = time.UTC
	beego.ErrorController(&utils.ErrorController{})
	beego.BConfig.ServerName = "DDC Patungan 1.0"
	beego.BConfig.Log.AccessLogs = true
	beego.Run()
}
