package controllers

import (
	"ddc_utils/utils"

	"github.com/astaxie/beego"
)

// Operations about object
type DefaultController struct {
	beego.Controller
}

type DefaultData struct {
	Application string `json:"application"`
	IPAddress   string `json:"ipAddress"`
	UserAgent   string `json:"userAgent"`
	Note        string `json:"note"`
}

// @Title Welcome information
// @Description API Welcome information
// @Success 200 {object}
// @router / [any]
func (dc *DefaultController) GetAll() {

	defaultData := DefaultData{
		Application: "DDC Utils 3.0",
		IPAddress:   dc.Ctx.Input.Domain(),
		UserAgent:   dc.Ctx.Request.Header.Get("User-Agent"),
	}
	dc.Data["json"] = utils.Response{Errmsg: "success.", Data: defaultData}
	dc.ServeJSON()
	return
}

// @Title Clear Cache
// @Description Clearing Order Cache
// @Success 200 {object}
// @router /cc [any]
func (dc *DefaultController) ClearCache() {

	_ = utils.GcCache.ClearAll()

	defaultData := DefaultData{
		Application: "DDC Utils 3.0",
		IPAddress:   dc.Ctx.Request.Header.Get("X-Forwarded-For"),
		UserAgent:   dc.Ctx.Request.Header.Get("User-Agent"),
		Note:        "CacheCleared",
	}

	dc.Data["json"] = utils.Response{Errcode: 200, Errmsg: "success.", Data: defaultData}
	dc.ServeJSON()
	return
}
