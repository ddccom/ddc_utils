package routers

import (
	"ddc_utils/controllers"

	"github.com/astaxie/beego"
)

// Use annotation routing

func init() {

	beego.Router("/", &controllers.DefaultController{}, "*:GetAll")

}
