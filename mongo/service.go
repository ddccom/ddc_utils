package mongo

import (
	"gopkg.in/mgo.v2"
	"log"
	"time"
)

type Service struct {
	baseSession *mgo.Session
	queue       chan int
	Address     string
	Username    string
	Password    string
	Open        int
	Debug       bool
	Logger      *log.Logger
}

var service Service

func (s *Service) New() error {
	var err error

	s.queue = make(chan int, maxPool)

	for i := 0; i < maxPool; i = i + 1 {
		s.queue <- 1
	}

	s.Open = 0

	s.baseSession, err = mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{s.Address},
		Username: s.Username,
		Password: s.Password,
		Timeout:  10 * time.Second,
	})

	if s.Debug && s.Logger != nil {
		mgo.SetDebug(true)
		mgo.SetLogger(s.Logger)
	}

	return err
}

func (s *Service) Session() *mgo.Session {
	<-s.queue
	s.Open++

	return s.baseSession.Copy()
}

func (s *Service) Close(c *Collection) {
	c.db.s.Close()
	s.queue <- 1
	s.Open--
}
