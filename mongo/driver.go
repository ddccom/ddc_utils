package mongo

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/rs/zerolog/log"
)

var maxPool int

func init() {
	var err error

	appConf := beego.AppConfig

	maxPool, err = appConf.Int("mongo::db_max_pool")

	if err != nil {
		println(err)
		logs.Error("Mongo maxPool setting error!!!")
		logs.Error(err.Error())
	}

	// init method to start db
	checkAndInitServiceConnection()
}

func checkAndInitServiceConnection() {
	appConf := beego.AppConfig

	if service.baseSession == nil {
		service.Address = fmt.Sprintf("%s:%s", appConf.String("mongo::db_host"), appConf.String("mongo::db_port"))
		service.Username = appConf.String("mongo::db_user")
		service.Password = appConf.String("mongo::db_password")

		if mgoDebug, err := appConf.Bool("mongo::db_debug"); err == nil && mgoDebug {
			service.Debug = true
			service.Logger = logs.GetLogger()
		}

		err := service.New()
		if err != nil {
			println(err)
			logs.Error("Mongo connection failed!!!")
			logs.Error(err.Error())
		} else {
			logs.Info("Initialized MongoDB Connection")
			log.Info().Msg("Initialized MongoDB Connection")
		}
	}
}
