package utils

import (
	"regexp"
	"strconv"
	"strings"
)

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")
var matchSnakeCase = regexp.MustCompile("(^[A-Za-z])|_([A-Za-z])")

// Convert camelCase to snake_case
func ToSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}

// Convert snake_case to camelCase
func ToCamelCase(str string) string {
	return matchSnakeCase.ReplaceAllStringFunc(str, func(s string) string {
		return strings.ToUpper(strings.Replace(s, "_", "", -1))
	})
}

//Join slice of strings into a single string
func JoinStrings(strs []string) string {

	if len(strs) == 0 {
		return ""
	}

	if len(strs) > 1 {
		return strings.Join(strs[:], "; ")
	} else {
		return strs[0]
	}
}

//Split string with separator, to slice of int
func SplitStringToSliceInt(A string, args ...string) []int {
	var err error
	var sep string

	if len(args) >= 1 && args[0] != "" {
		sep = args[0]
	} else {
		sep = ","
	}

	a := strings.Split(A, sep)
	n := len(a)
	b := make([]int, n)
	for i, v := range a {
		b[i], err = strconv.Atoi(v)
		if err != nil {
			DDCPushLog(err.Error())
			//proper err handling
			//either b[i] = -1 (in case positive integers)
		}
	}
	return b
}
