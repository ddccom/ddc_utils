package utils

import (
	"net"
	"net/http"
	"time"
)

var (
	DdcHttpT     *http.Transport
	DdcNetClient *http.Client
)

func init() {
	DdcHttpT = &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 100 * time.Second,
		}).DialContext,
		MaxIdleConnsPerHost:   100,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 3 * time.Second,
	}

	DdcNetClient = &http.Client{
		Transport: DdcHttpT,
		Timeout:   5 * time.Second,
	}
}
