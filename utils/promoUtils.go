package utils

import (
	"reflect"
	"time"
)

//GetCurrentTime ...
func GetCurrentTime() time.Time {
	loc, _ := time.LoadLocation("Asia/Jakarta")
	return time.Now().In(loc)
}

//IntInSlice ...
func IntInSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func NewNullInt(v int) interface{} {
	if v == 0 {
		return nil
	}
	return v
}

func InArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}

	return
}

//use a more hack-y way: using integer division to get rid of the remaining and then multiply back
func CountDaysDiff(t1, t2 time.Time) int {
	d1 := t1.Unix() / 86400 * 86400
	return int((t2.Unix() - d1) / 86400)
}
