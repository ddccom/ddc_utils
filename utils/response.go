package utils

import (
	"io"
	"log"
	"time"
)

var (
	//timeout for request to another service
	ServiceRequestTimeout time.Duration
)

//Response Structural body
type Response struct {
	Errcode int         `json:"code"`
	Errmsg  string      `json:"message"`
	Data    interface{} `json:"data"`
}

//Response Error Structural body
type ErrResponse struct {
	Errcode int         `json:"code"`
	Errmsg  interface{} `json:"message"`
}

func init() {
	ServiceRequestTimeout = 5 * time.Second
}

//ResponseClose Close response but check errors, used for defer statement
func ResponseClose(c io.Closer) {
	err := c.Close()
	if err != nil {
		log.Fatal(err)
	}
}
