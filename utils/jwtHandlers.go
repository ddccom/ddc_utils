package utils

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/astaxie/beego/context"
)

func init() {
}

func Jwt(this *context.Context) {
	et := EasyToken{}
	authtoken := strings.TrimSpace(this.Request.Header.Get("Authorization"))
	valido, err := et.ValidateToken(authtoken)
	if !valido {
		this.Output.SetStatus(200)
		this.Output.Header("Content-Type", "application/json")
		resBody, _ := json.Marshal(ErrResponse{401, fmt.Sprintf("%s", err)})
		this.Output.Body(resBody)
		return
	}

}
