package utils

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"log"
	"os"
	"strings"

	"github.com/astaxie/beego/config"
)

const (
	ErrAuthNotFound = "Authorization Not Found"
)

// DDCLog DDC Logging to file
func DDCLog(rawMsg interface{}, appName ...string) {
	var message string

	appConf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		panic(err)
	}

	var filename string
	if len(appName) >= 1 && len(appName[0]) > 1 {
		filename = appName[0]
	} else {
		filename = appConf.String("appname")
	}

	var logType string
	if len(appName) >= 2 && len(appName[1]) > 1 && appName[1] == "DEBUG" {
		logType = appName[1]
		message = logType + " : "
	}

	f, err := os.OpenFile("logs/"+filename+".log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	defer ResponseClose(f)

	if err != nil {
		log.Fatal(err)
		return
	}

	message += formatLog(rawMsg)

	logger := log.New(f, "", log.LstdFlags)
	logger.Println(message)

	return
}

func DDCPushLog(message string) {
	logs.Error("DDCPushLog : ", message)
	DDCLog(message)
}

func DDCPushDebugLog(message string) {
	logs.Debug("DDCPushLog : ", message)
	DDCLog(message, "", "DEBUG")
}

func formatLog(f interface{}, v ...interface{}) string {
	var msg string
	switch f.(type) {
	case string:
		msg = f.(string)
		if len(v) == 0 {
			return msg
		}
		if strings.Contains(msg, "%") && !strings.Contains(msg, "%%") {
			//format string
		} else {
			//do not contain format char
			msg += strings.Repeat(" %v", len(v))
		}
	default:
		msg = fmt.Sprint(f)
		if len(v) == 0 {
			return msg
		}
		msg += strings.Repeat(" %v", len(v))
	}
	return fmt.Sprintf(msg, v...)
}
