package utils

// PtgMessages store common validate template
var PtgMessages = map[string]string{
	"PtgDisabled":             "Patungan tidak diperkenankan/tersedia.",
	"PtgInvalid":              "Patungan tidak ada/expired.",
	"PtgIsInvite":             "Patungan proses mengundang.",
	"PtgIsCheckout":           "Patungan menunggu pembayaran.",
	"PtgIsOrder":              "Patungan masuk transaksi.",
	"PtgIsCanceled":           "Patungan dibatalkan sistem.",
	"PtgIsRejected":           "Undangan Patungan ditolak.",
	"PtgCheckoutNotValid":     "Patungan belum dapat diproses untuk pembayaran.",
	"PtgInvalidMember":        "Fitur Patungan hanya untuk member Reseller saja.",
	"PtgInvalidBranch":        "Fitur Patungan hanya untuk Gudang saja.",
	"PtgMinTotalQty":          "Kuantitas minimum produk patungan : ",
	"PtgMaxTotalQty":          "Kuantitas maximum produk patungan : ",
	"PtgFixTotalQty":          "Kuantitas produk patungan yang diperbolehkan : ",
	"PtgInvalidTotalQty":      "Kuantitas total patungan tidak valid.",
	"PtgTotalQtyLessThanQty":  "Kuantitas pembelian tidak boleh melebihi total patungan.",
	"PtgInvalidQty":           "Kuantitas patungan tidak valid.",
	"PtgInvalidNewQty":        "Kuantitas patungan harus lebih dari : ",
	"PtgStockUnavailable":     "Stok tidak tersedia.",
	"PtgUpdateFailed":         "Update Patungan gagal.",
	"PtgMemberCannotCheckout": "Pembayaran patungan tidak dapat dilakukan. (telah dibayar/tidak valid)",
	"PtgMemberCannotInvite":   "Anda tidak diizinkan untuk mengundang member lain.",
	"PtgMemberHasQueueOrder":  "Transaksi sedang dalam proses. Mohon menunggu.",
	"PtgMemberHasCart":        "Data di keranjang patungan telah ada.",
	"PtgMemberLimit":          "Jumlah slot member patungan telah penuh.",
	"PtgMemberIsInitiator":    "Member ini adalah initiator, tidak diperkenankan untuk bergabung kedalam Patungan.",
	"PtgMemberInvalid":        "Member tidak valid.",
	"PtgMemberInvited":        "Member ini telah diundang.",
	"PtgMemberJoined":         "Member ini telah bergabung.",
	"PtgMemberRejected":       "Member ini telah menolak undangan Patungan sebelumnya.",
	"PtgMemberCannotJoin":     "Member ini tidak diperbolehkan bergabung.",
	"PtgMemberJoinSuccess":    "Member sukses bergabung.",
	"PtgMemberRejectSuccess":  "Member menolak undangan Patungan.",
	"PtgMemberDeclineEdit":    "Patungan Member tidak bisa di edit.",
	"PtgMemberCanEditQtyDone": "Permintaan update Qty sudah dilakukan.",
	"PtgMemberIsInvited":      "Patungan invite proses mengundang.",
	"PtgMemberIsJoined":       "Patungan invite telah bergabung.",
	"PtgMemberIsReject":       "Patungan invite ditolak.",
	"PtgMemberIsCheckout":     "Patungan invite menunggu pembayaran.",
	"PtgUnauthorized":         "Unauthorized.",
	"PtgExpiredInvitation":    "Batas waktu telah berakhir.",
	"PtgExpiredCheckout":      "Batas waktu telah berakhir.",
}

var PtgMemberTypeAllowed = []int{2}

const (
	PtgEmailInvitationTmpId = 185
	PtgServiceFee           = 5000

	// Patungan Status :
	// 0:pending invitation, 1:pending checkout, 2:all checkout finished/in order
	PtgStateInvite          = 0   //pending invitation
	PtgStateCheckout        = 1   //pending checkout
	PtgStateOrder           = 2   //in order
	PtgStateCanceled        = 3   //canceled
	PtgStateLocked          = 4   //locked
	PtgTimeLimitInvitation  = 60  //minutes
	PtgTimeLimitCheckout    = 120 //minutes
	PtgMemberStatusInvited  = 0
	PtgMemberStatusJoined   = 1
	PtgMemberStatusReject   = 2
	PtgMemberStatusCheckout = 3
	PtgNumMember            = 2 //number of join member
	PtgMaxMember            = 3 //number of total member including initiator
	PtgOrderTypeId          = 8 //Patungan OrderTypeId

	PtgMemberLevelId = 4 //patungan get lowest tier price
	NumCanceledPtg   = 3 //number of canceled patungan

	WebSettNamePtgEnable     = "ptg_enable"
	WebSettNamePtgPerProduct = "ptg_per_product"
)

//PtgMemberTypeIsAllowed
func PtgMemberTypeIsAllowed(memberTypeId int) (valid bool) {
	return memberTypeId > 0 && len(PtgMemberTypeAllowed) > 0 && IntInSlice(memberTypeId, PtgMemberTypeAllowed)
}

//PtgStatusStr
//args[0] : expiredInvite
//args[1] : expiredCheckout
func PtgStatusStr(ptgStatus int, args ...interface{}) (statusStr string) {
	var expiredInvite, expiredCheckout bool
	if len(args) >= 1 {
		if v, ok := args[0].(bool); ok {
			expiredInvite = v
		}

		if len(args) >= 2 {
			if v, ok := args[1].(bool); ok {
				expiredCheckout = v
			}
		}
	}

	switch ptgStatus {
	case PtgStateInvite:
		statusStr = PtgMessages["PtgIsInvite"]

		if expiredInvite {
			statusStr = PtgMessages["PtgExpiredInvitation"]
		}
	case PtgStateCheckout:
		statusStr = PtgMessages["PtgIsCheckout"]

		if expiredCheckout {
			statusStr = PtgMessages["PtgExpiredCheckout"]
		}
	case PtgStateOrder:
		statusStr = PtgMessages["PtgIsOrder"]
	case PtgStateCanceled:
		statusStr = PtgMessages["PtgIsCanceled"]
	case PtgStateLocked:
		statusStr = PtgMessages["PtgIsCanceled"]
	}

	return statusStr
}

//PtgParseArgs
//args[0] : isPatungan		bool	false
//args[1] : patunganUuid	string	""
func PtgParseArgs(args []interface{}) (isPatungan bool, ptgUuid string) {
	if len(args) >= 1 {
		if v, ok := args[0].(bool); ok {
			isPatungan = v
		}

		if len(args) >= 2 {
			if v, ok := args[1].(string); ok {
				ptgUuid = v
			}
		}
	}

	return
}

//PtgMemberStatusStr
func PtgMemberStatusStr(ptgMemberStatus int) (statusStr string) {
	switch ptgMemberStatus {
	case PtgMemberStatusInvited:
		statusStr = PtgMessages["PtgMemberIsInvited"]
	case PtgMemberStatusJoined:
		statusStr = PtgMessages["PtgMemberIsJoined"]
	case PtgMemberStatusReject:
		statusStr = PtgMessages["PtgMemberIsReject"]
	case PtgMemberStatusCheckout:
		statusStr = PtgMessages["PtgMemberIsCheckout"]
	}

	return statusStr
}
