package utils

import (
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/astaxie/beego"
)

// checkAuth Check Authentication
//args[0] : payload io.Reader
func CheckAuth(authToken string, args ...interface{}) (ret string, err error) {
	var payload io.Reader
	if len(args) >= 1 {
		if val, ok := args[0].(io.Reader); ok {
			payload = val
		}
	}

	url := beego.AppConfig.String("server::ddc_authentication") + "v3/member/authMember"

	ctx, cancel := context.WithTimeout(context.Background(), ServiceRequestTimeout)
	defer cancel()

	req, err := http.NewRequest("GET", url, payload)
	if err != nil {
		DDCPushLog("CheckAuth : " + err.Error())
		bt, _ := json.Marshal(ErrResponse{Errcode: 400, Errmsg: err.Error()})
		return string(bt), err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", authToken)

	res, err := DdcNetClient.Do(req.WithContext(ctx))

	if res != nil {
		defer ResponseClose(res.Body) // MUST CLOSED THIS
	}

	if err != nil {
		DDCPushLog("CheckAuth : " + err.Error())
		bt, _ := json.Marshal(ErrResponse{Errcode: 400, Errmsg: err.Error()})
		return string(bt), err
	}

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		DDCPushLog("CheckAuth : " + err.Error())
		bt, _ := json.Marshal(ErrResponse{Errcode: 400, Errmsg: err.Error()})
		return string(bt), err
	}

	return string(body), nil
}
