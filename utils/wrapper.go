package utils

import (
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"runtime"
)

type WrapFnType func(m interface{}) (err error)

//WrapFunctionHandler Wrap function for error handling
func WrapFunctionHandler(function WrapFnType, m interface{}) (err error) {
	defer recoverPanic()

	err = function(m)
	return
}

// recoverPanic custom panic recovery
//@see vendor/github.com/astaxie/beego/config.go:161
func recoverPanic() {
	if err := recover(); err != nil {
		if err == beego.ErrAbort {
			return
		}
		if !beego.BConfig.RecoverPanic {
			panic(err)
		}

		logs.Critical("Handler crashed with error", err)
		for i := 1; ; i++ {
			_, file, line, ok := runtime.Caller(i)
			if !ok {
				break
			}
			logs.Critical(fmt.Sprintf("%s:%d", file, line))
		}
	}
}
