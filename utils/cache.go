package utils

import (
	"errors"
	"github.com/astaxie/beego/cache"
	"github.com/astaxie/beego/config"
	"time"
)

var GcCache CustomCache

type CustomCache struct {
	C      cache.Cache
	Str    string
	Time   time.Duration
	Active bool
}

func init() {
	var err error
	var appConf config.Configer
	var cacheEnable bool
	var cacheTime int

	appConf, err = config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		panic(err)
	}

	//enable cache or not
	cacheEnable, err = appConf.Bool("cacheEnable")
	if err != nil {
		cacheEnable = true
	}

	//time to keep the cache in seconds
	cacheTime, err = appConf.Int("cacheTime")
	if err != nil {
		//default cacheTime
		cacheTime = 3
	}

	//GC interval in seconds
	GcCache.C, _ = cache.NewCache("memory", `{"interval":60}`)

	//time to keep the cache
	GcCache.Time = time.Duration(cacheTime) * time.Second

	//Is Global GcCache Active?
	GcCache.Active = cacheEnable
}

//key : [0] : key, [1] : timeout or vice versa
func (u *CustomCache) Put(val interface{}, key ...interface{}) error {
	strKey := u.GetStrKey(key...)

	if strKey == "" {
		return errors.New("no cache key")
	}

	timeout := u.GetTimeout(key...)
	return u.C.Put(strKey, val, timeout)
}

func (u *CustomCache) Get(key ...interface{}) interface{} {
	strKey := u.GetStrKey(key...)

	if strKey == "" {
		return nil
	}

	return u.C.Get(strKey)
}

func (u *CustomCache) IsExist(key ...interface{}) bool {
	strKey := u.GetStrKey(key...)

	if strKey == "" {
		return false
	}

	return u.C.IsExist(strKey)
}

func (u *CustomCache) ClearAll() error {
	if !u.IsActive() {
		return errors.New("cache disabled")
	}

	return u.C.ClearAll()
}

func (u *CustomCache) Delete(key ...interface{}) error {
	strKey := u.GetStrKey(key...)

	if strKey == "" {
		return errors.New("no cache key")
	}

	return u.C.Delete(strKey)
}

//Get cache key, default : CustomCacheKey
func (u *CustomCache) GetStrKey(key ...interface{}) string {

	if !u.IsActive() {
		return ""
	}

	strKey := u.Str
	for _, arg := range key {
		if v, ok := arg.(string); ok {
			return v
		}
	}

	return strKey
}

//Get timeout argument, default : CustomCacheKey
func (u *CustomCache) GetTimeout(args ...interface{}) time.Duration {
	timeout := u.Time
	for _, arg := range args {
		if v, ok := arg.(time.Duration); ok {
			return v
		}
	}

	return timeout
}

func (u *CustomCache) IsActive() bool {
	return u.Active
}
