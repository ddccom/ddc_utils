package utils

import (
	"fmt"
	"math/big"
	"strconv"
	"time"

	"github.com/pborman/uuid"
	"strings"
)

const (
	alphabet = "23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
	shortlen = 22 //This length is derived from the alphabet, omitting the calculation steps
)

func ShortenUUID(s string) (ss string) {
	//uuInt, tr, tm, off := new(big.Int), new(big.Int), new(big.Int), new(big.Int)
	uuInt, off := new(big.Int), new(big.Int)
	//remove "-"
	s = strings.ToLower(strings.Replace(s, "-", "", -1))
	//fmt.Sscan("0x"+s, uuInt)
	uuInt.SetString(s, 16)

	alphaLen := big.NewInt(int64(len(alphabet)))
	for uuInt.Cmp(big.NewInt(0)) > 0 {
		//uuInt, off = tr.DivMod(uuInt, alphaLen, tm)
		uuInt, off = uuInt.DivMod(uuInt, alphaLen, off)
		ss += string(alphabet[off.Int64()])
	}
	//If less than 22 digits, complete with the first character
	if diff := shortlen - len(ss); diff > 0 {
		ss += strings.Repeat(string(alphabet[0]), diff)
	}

	return
}

func LengthenUUID(s string) (ls string) {
	uuInt, off := new(big.Int), new(big.Int)
	alphaLen := big.NewInt(int64(len(alphabet)))
	//Need reverse order
	for i := len(s) - 1; i >= 0; i-- {
		char := s[i]
		off = big.NewInt(int64(strings.Index(alphabet, string(char))))
		uuInt = uuInt.Add(uuInt.Mul(uuInt, alphaLen), off)
	}
	//Change to hexadecimal
	if b := fmt.Sprintf("%x", uuInt); b != "" {
		//fmt.Println(b)
		//b := []byte(b)
		ll := len(b)
		//Consider the case where the prefix is zero. In reverse, first satisfy the number of characters after the first
		ls = fmt.Sprintf("%08v-%04v-%04v-%04v-%012v",
			b[:ll-24], b[ll-24:ll-20], b[ll-20:ll-16], b[ll-16:ll-12], b[ll-12:])
	}
	return
}

//default, version 4
func NewUUID() string {
	return uuid.New()
}

func NewShortUUID() string {
	newUUID := NewUUID()
	return ShortenUUID(newUUID)
}

// generate uuid v5
// namespace Write domain name or url directly
func NewUUID5(namespace, data string) string {
	//URLs that start with http are URLs, and the rest are DNS
	if strings.HasPrefix(strings.ToLower(namespace), "http") {
		return uuid.NewSHA1(uuid.NameSpace_URL, []byte(namespace+data)).String()
	} else {
		return uuid.NewSHA1(uuid.NameSpace_DNS, []byte(namespace+data)).String()
	}
}

func NewShortUUID5(namespace, data string) string {
	newUUID := NewUUID5(namespace, data)
	return ShortenUUID(newUUID)
}

func PaymentNumberUUID(totalOrder string) (string, string, string) {
	today := time.Now()
	year := strconv.Itoa(today.Year())
	year = year[2:len(year)]
	month := strconv.Itoa(int(today.Month()))
	if len(month) == 1 {
		month = "0" + month
	}
	day := strconv.Itoa(int(today.Day()))
	if len(day) == 1 {
		day = "0" + day
	}
	hour := strconv.Itoa(int(today.Hour()))
	if len(hour) == 1 {
		hour = "0" + hour
	}
	minute := strconv.Itoa(int(today.Minute()))
	if len(minute) == 1 {
		minute = "0" + minute
	}
	second := strconv.Itoa(int(today.Second()))
	if len(second) == 1 {
		second = "0" + second
	}

	prefixNumber := day + month + year
	paymentNumber := "DDC" + prefixNumber + "-" + totalOrder
	uuid := NewUUID5("dusdusan.com", paymentNumber)
	return paymentNumber, uuid, prefixNumber
}

func DayMonthYear() (string, string, string) {
	today := time.Now()
	year := strconv.Itoa(today.Year())
	year = year[2:len(year)]
	month := strconv.Itoa(int(today.Month()))
	if len(month) == 1 {
		month = "0" + month
	}
	day := strconv.Itoa(int(today.Day()))
	if len(day) == 1 {
		day = "0" + day
	}
	return day, month, year
}

func DateFull() string {
	today := time.Now()
	years := strconv.Itoa(today.Year())

	year := strconv.Itoa(today.Year())
	year = year[2:len(year)]
	month := strconv.Itoa(int(today.Month()))
	if len(month) == 1 {
		month = "0" + month
	}
	day := strconv.Itoa(int(today.Day()))
	if len(day) == 1 {
		day = "0" + day
	}
	hour := strconv.Itoa(int(today.Hour()))
	if len(hour) == 1 {
		hour = "0" + hour
	}
	minute := strconv.Itoa(int(today.Minute()))
	if len(minute) == 1 {
		minute = "0" + minute
	}
	second := strconv.Itoa(int(today.Second()))
	if len(second) == 1 {
		second = "0" + second
	}
	date := years + "-" + month + "-" + day
	return date
}
