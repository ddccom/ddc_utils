package utils

import (
	"errors"
	"log"
	"strings"

	"github.com/astaxie/beego/config"
	"github.com/dgrijalva/jwt-go"
)

type EasyToken struct {
	Email   string
	Uid     int
	Expires int64
}

var (
	verifyKey  string
	ErrAbsent  = "Token does not exist" // Token does not exist
	ErrInvalid = "Invalid token"        // Invalid token
	ErrExpired = "Token expired"        // Token expired
	ErrOther   = "Other errorsr"        // Other errors
)

func init() {
	appConf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		panic(err)
	}
	verifyKey = appConf.String("jwt::token")
}

func (e EasyToken) GetToken() (string, error) {
	claims := &jwt.StandardClaims{
		ExpiresAt: e.Expires, //time.Unix(c.ExpiresAt, 0)
		Issuer:    e.Email,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(verifyKey))
	if err != nil {
		log.Println(err)
	}
	return tokenString, err
}

func (e EasyToken) ValidateToken(tokenString string) (bool, error) {
	if tokenString == "" {
		return false, errors.New(ErrAbsent)
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(verifyKey), nil
	})
	if token == nil {
		return false, errors.New(ErrInvalid)
	}
	if token.Valid {

		return true, nil
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return false, errors.New(ErrInvalid)
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			return false, errors.New(ErrExpired)
		} else {
			return false, errors.New(ErrOther)
		}
	} else {
		return false, errors.New(ErrOther)
	}
}

//Parse tokenString and filter out "Bearer" prefix if exists
func FilterBearer(tokenString *string) {
	if strings.HasPrefix(*tokenString, "Bearer") {
		splitString := strings.Split(*tokenString, "Bearer")
		if len(splitString) >= 2 {
			*tokenString = strings.TrimSpace(splitString[1])
		}
	}
}
