package utils

import (
	"encoding/json"
	"github.com/mitchellh/mapstructure"
)

func Decode(input interface{}, output interface{}, args ...bool) (err error) {
	var usingDecoder bool

	if len(args) > 0 {
		usingDecoder = args[0]
	} else {
		usingDecoder = false
	}

	if usingDecoder {
		//Decoder
		config := &mapstructure.DecoderConfig{
			Metadata: nil,
			Result:   output,
		}

		decoder, err := mapstructure.NewDecoder(config)
		if err != nil {
			return err
		}

		return decoder.Decode(input)
	} else {
		// convert map to json
		jsonString, err := json.Marshal(input)
		if err != nil {
			return err
		}

		// convert json to struct
		err = json.Unmarshal(jsonString, output)
		if err != nil {
			return err
		}
	}

	return err
}
