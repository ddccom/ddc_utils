package utils

import (
	"fmt"
	"net/url"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/config"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// Database connection initialization
func Init() {
	appConf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		panic(err)
	}

	conn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s",
		appConf.String("database::db_user"),
		appConf.String("database::db_password"),
		appConf.String("database::db_host"),
		appConf.String("database::db_port"),
		appConf.String("database::db_name"),
		appConf.String("database::db_charset"))

	timezone := appConf.String("database::db_timezone")
	if timezone != "" {
		conn = conn + "&loc=" + url.QueryEscape(timezone)
	}

	orm.RegisterDataBase("default", "mysql", conn)

	defaultDB, err := orm.GetDB()
	if err != nil {
		beego.Emergency("Get default DB error:" + err.Error())
	}

	maxIdleConn, _ := appConf.Int("database::maxIdleConn")
	maxOpenConn, _ := appConf.Int("database::maxOpenConn")
	maxLifeTime, _ := appConf.Int64("database::maxLifeTime")
	maxLifeTimeDuration := time.Duration(maxLifeTime)

	if maxIdleConn == 0 {
		maxIdleConn = 20
	}

	if maxLifeTimeDuration == 0 {
		maxLifeTimeDuration = 50
	}

	defaultDB.SetMaxIdleConns(maxIdleConn)
	defaultDB.SetMaxOpenConns(maxOpenConn)
	defaultDB.SetConnMaxLifetime(time.Minute * maxLifeTimeDuration)

	if beego.AppConfig.String("runmode") == "dev" {
		orm.Debug = true
	}
}

//Return the prefixed table name
func TableName(str string) string {
	appConf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		panic(err)
	}
	return appConf.String("database::db_prefix") + str
}
