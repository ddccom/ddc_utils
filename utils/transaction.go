package utils

import (
	"github.com/astaxie/beego/orm"
)

func CheckOrmInArgs(args []interface{}) (o orm.Ormer, hasOrm bool) {
	if len(args) >= 1 {
		if val, ok := args[0].(orm.Ormer); ok {
			return val, true
		}
	}

	o = orm.NewOrm()
	return o, false
}
